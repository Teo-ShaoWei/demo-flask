import flask
import os
import os.path
import sqlite3
from random import randint


LOWER = 1
UPPER = 100
INITIAL_STATE = {'round': 0,
                 'guess': LOWER,
                 'lower': LOWER,
                 'upper': UPPER,
                 'result': 'start'}
DATABASE_FILE = 'db.sqlite3'



app = flask.Flask(__name__)


@app.route('/')
def home():
    return flask.render_template('index.html', lower = LOWER, upper = UPPER)


@app.route('/start')
def start_game():
    refresh_db()
    value = pick_random_number(LOWER, UPPER)
    set_picked(value)
    insert_state(INITIAL_STATE)
    return flask.redirect(flask.url_for('make_guess'))


@app.route('/game')
def make_guess():
    state = get_last_state()
    result_comment = flask.render_template(os.path.join('partials', state['result'] + '.html'), guess = state['guess'])
    return flask.render_template('game.html', result_comment = result_comment, state = state)


@app.route('/guessed', methods=['POST'])
def process_guess():
    guess = int(flask.request.form['guess'])
    value = get_picked()
    state = dict(get_last_state())
    update(guess, value, state)

    if state['result'] == 'hit':
        return flask.redirect(flask.url_for('end_game'))
    else:
        return flask.redirect(flask.url_for('make_guess'))


@app.route('/result')
def end_game():
    value = get_picked()
    states = get_states()[1:]
    return flask.render_template('result.html', value = value, states = states)



def get_db():
    db = sqlite3.connect(DATABASE_FILE)
    db.row_factory = sqlite3.Row
    return db


def create_db():
    db = get_db()
    db.execute('CREATE TABLE picked ' +
               '(value INTEGER NOT NULL)')
    db.execute('CREATE TABLE states ' +
               '(round INTEGER PRIMARY KEY, ' +
               'guess INTEGER NOT NULL, ' +
               'lower INTEGER NOT NULL, ' +
               'upper INTEGER NOT NULL, ' +
               'result VARCHAR(10))')
    db.close()


def refresh_db():
    db = get_db()
    db.execute('DELETE FROM picked')
    db.execute('DELETE FROM states')
    db.commit()
    db.close()


def set_picked(value):
    db = get_db()
    db.execute('INSERT INTO picked ' +
               'VALUES(?)',
               (value,))
    db.commit()
    db.close()


def get_picked():
    db = get_db()
    result = db.execute('SELECT * FROM picked').fetchone()['value']
    db.close()
    return result


def insert_state(state):
    db = get_db()
    db.execute('INSERT INTO states ' +
               'VALUES(?, ?, ?, ?, ?)',
               (state['round'], state['guess'], state['lower'], state['upper'], state['result']))
    db.commit()
    db.close()


def get_states():
    db = get_db()
    result = db.execute('SELECT * FROM states').fetchall()
    db.close()
    return result


def get_last_state():
    states = get_states()
    return states[-1]



def pick_random_number(lower, upper):
    return randint(lower, upper + 1)


def update(guess, value, state):
    state['round'] += 1
    state['guess'] = guess

    if guess < state['lower'] or guess > state['upper']:
        state['result'] = 'invalid'
    elif guess == value:
        state['result'] = 'hit'
    elif guess < value:
        state['lower'] = guess + 1
        state['result'] = 'smaller'
    else:
        state['upper'] = guess - 1
        state['result'] = 'larger'

    insert_state(state)



if __name__ == '__main__':
    if not os.path.isfile(DATABASE_FILE):
        create_db()

    app.run(port=12345, debug=True)
